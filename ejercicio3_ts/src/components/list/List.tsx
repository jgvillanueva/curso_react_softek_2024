import {FC, ReactElement} from "react";
import Item from "../item/Item";
// import PropTypes from "prop-types";

type IProps = {
	data: IUsuario[],
	title: string,
	handleClick: Function
}

export interface IUsuario {
	nombre: string,
	edad: number
}

const List: FC<IProps> = (props): ReactElement => {
	const usuarios = props.data.map((usuario, index) => {
		return <div
			onClick={() => {props.handleClick(usuario)}}
			key={index}
		>
			<Item
				nombre={usuario.nombre}
				edad={usuario.edad}
			/>
		</div>
	})

	return (
		<div data-testid="list">
			<h2>{props.title}</h2>
			{usuarios}
		</div>
	)
}


/*
function validaUsuarios(props: any, propName: string) {
	const valor = props.data;
	if (valor && valor.length > 0) {
		return null
	}
	return Error('La lista de usuarios no existe o no tiene contenido');
}

List.defaultProps = {
	title: '',
	data: [{nombre: 'No hay datos', edad: 0}],
}

List.propTypes = {
	title: PropTypes.string.isRequired,
	data: validaUsuarios,
}
*/
export default List;
