import List, {IUsuario} from "./List";
import {render, screen} from "@testing-library/react";

const usuarios: IUsuario[] = [
	{nombre: 'Jorge', edad: 73},
	{nombre: 'Juan', edad: 32},
	{nombre: 'Ana', edad: 43},
	{nombre: 'Carlos', edad: 12},
];

describe('List component', () => {
	it('renders list with all item', () => {
		render(<List data={usuarios} title='título' handleClick={() => {}} />);
		const items = screen.queryAllByTestId('item');
		expect(items.length).toBe(usuarios.length);
	});
})
