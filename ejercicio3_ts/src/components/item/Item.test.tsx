import React from 'react';
import {render, screen, within} from '@testing-library/react';
import Item from "./Item";
import {IUsuario} from "../list/List";

const usuario: IUsuario = {
	nombre: 'Jorge',
	edad: 5,
}

describe('Item component', () => {
	beforeEach(() => {
		render(<Item nombre={usuario.nombre} edad={usuario.edad} />);
	})

	it('render item component as li', () => {
		const li = screen.getByTestId('item');
		expect(li).toBeInstanceOf(HTMLLIElement);
	});

	it('render nombre', () => {
		const itemNombre = screen.getByText(usuario.nombre);
		expect(itemNombre).toBeInTheDocument();
	});

})
