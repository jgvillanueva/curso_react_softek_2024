import {FC, ReactElement} from "react";
import {IUsuario} from "../list/List";

const Item: FC<IUsuario> = (props): ReactElement => {
	return (
		<li data-testid="item">
			{props.nombre}
		</li>
	)
}

export default Item;
