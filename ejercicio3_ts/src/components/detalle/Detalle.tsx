import {FC, ReactElement} from "react";
import {IUsuario} from "../list/List";

type IProps = {
	nombre: string,
	edad?: number,
}
const Detalle: FC<IProps> = (props): ReactElement => {
	/*let edad;
	if (props.edad) {
		edad = ` con edad ${props.edad}`;
	}*/

	const getEdad = () => {
		if (!props.edad)
			return null;
		return <span data-testid="edad"> con edad {props.edad}</span>;
	}

	return (
		<div className="block p-6 bg-white border border-gray-200 rounded-lg shadow m-5 text-black max-w-sm">
			<h1 data-testid="detalle">
				{props.nombre}
				{/*
			{edad}
			*/}
				{/*
				props.edad?
					<span data-testid="edad">con edad {props.edad}</span>
					:
					null
			*/}
				{
					getEdad()
				}
			</h1>
		</div>

	)
}

export default Detalle;
