import React from 'react';
import {render, screen, within} from '@testing-library/react';
import Detalle from "./Detalle";

const nombre = 'Jorge';
const edad = 23;

describe('Detalle component', () => {
	it('renders nombre y edad', () => {
		render(<Detalle nombre={nombre} edad={edad} />);
		const detalleNombre = screen.getByText(nombre);
		expect(detalleNombre).toBeInTheDocument();
		const detalleEdad = screen.getByTestId('edad');
		expect(detalleEdad).toHaveTextContent(edad.toString());
	})

	it('renders only nombre', () => {
		render(<Detalle nombre={nombre}  />);
		const detalleNombre = screen.getByText(nombre);
		expect(detalleNombre).toBeInTheDocument();
		const detalleEdad = screen.queryAllByTestId('edad');
		expect(detalleEdad.length).toBe(0);
	})
})
