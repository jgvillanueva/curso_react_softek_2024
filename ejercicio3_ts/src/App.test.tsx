import React from 'react';
import {fireEvent, render, screen, within} from '@testing-library/react';
import App from './App';

describe('App component', () => {
  beforeEach(() => {
    render(<App />);
  })

  it('renders the h1', () => {
    const title = screen.getByText(/Curso de React/i);
    expect(title).toBeInTheDocument();
    expect(title).toBeInstanceOf(HTMLHeadingElement);
  });

  it('render h1 inside header', () => {
    const header = screen.getByTestId('header');
    const title = within(header).getByText(/Curso de React/i);
    expect(title).not.toBeNull();
  });

  it('render component inside header', () => {
    const header = screen.getByTestId('header');
    const list = within(header).getByTestId('list');
    expect(list).not.toBeNull();
    const detalle = within(header).getByTestId('detalle');
    expect(detalle).not.toBeNull();
  });

  it('item click load detail data', () => {
    const items = screen.queryAllByTestId('item');
    const item = items[0];
    fireEvent.click(item);
    const detalle = screen.getByTestId('detalle');
    const texto = item.textContent ? item.textContent : '';
    expect(detalle).toHaveTextContent(texto);
    expect(detalle).not.toHaveTextContent('Juan');
  });
})
