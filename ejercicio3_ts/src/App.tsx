import React, {useState} from 'react';
import './App.css';
import List, {IUsuario} from "./components/list/List";
import Detalle from "./components/detalle/Detalle";
import {estilos} from "./tStyles";

function App() {
    const usuarios: IUsuario[] = [
        {nombre: 'Jorge', edad: 73},
        {nombre: 'Juan', edad: 32},
        {nombre: 'Ana', edad: 43},
        {nombre: 'Carlos', edad: 12},
    ];

    const [nombre, setNombre] = useState<string>('Nadie seleccionado');
    const [edad, setEdad] = useState<number | undefined>();

    const receiveClick = (valor: IUsuario): void => {
        setNombre(valor.nombre);
        setEdad(valor.edad);
    }

    return (
        <div className="App bg-gray-800 px-15 xs:px-10 lg:px-4 m-5 flex ">
            <header className="App-header text-white" data-testid="header">
                <h1 className={estilos.classH1}>Curso de React</h1>
                <List
                    data={usuarios}
                    title="Hola TS"
                    handleClick={receiveClick}
                />
                <Detalle nombre={nombre} edad={edad} />
            </header>
        </div>
    );
}

export default App;
