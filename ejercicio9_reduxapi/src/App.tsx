import React from 'react';
import './App.css';
import {New} from "./components/New";
import {Lista} from "./components/Lista";

function App() {
  return (
    <div className="App">
        <h1>Redux rtk api</h1>
      <New />
      <Lista />
    </div>
  );
}

export default App;
