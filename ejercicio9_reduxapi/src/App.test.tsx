import React from 'react';
import {render, screen, within} from '@testing-library/react';
import App from './App';
import {Provider} from "react-redux";
import {store} from "./redux/store";

describe('App component', () => {
  it('renders learn react link', () => {
    render(<Provider store={store}><App /></Provider>);
    const linkElement = screen.getByText(/Redux rtk api/i);
    expect(linkElement).toBeInTheDocument();
  });

  it('carga los mensajes, etc', async () => {
    render(<Provider store={store}><App /></Provider>);
    const listElement = screen.getByTestId('lista');
    expect(listElement).toBeInTheDocument();
    const mensajes =
        await within(listElement).findAllByTestId('mensaje');
    expect(mensajes.length).toBeGreaterThan(0);
  });
})

