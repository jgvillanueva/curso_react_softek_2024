import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import dataService, {Mensaje} from "../services/data.service";
import {RootState} from "./store";

const initialState: MensajesState = {
	listaMensajes: [],
	loading: false,
	error: '',
	result: '',
}

interface RtkState {
	error: string,
	loading: boolean,
	result: string,
}

export interface MensajesState extends RtkState{
	listaMensajes: Mensaje[],
}

export const getMensajes = createAsyncThunk(
	'getMensajes',
	async () => {
		const response = await dataService.getAll();
		return response.json();
	}
);

export const addMensaje = createAsyncThunk(
	'addMensaje',
	async (mensaje: Mensaje) => {
		const response = await dataService.addMensaje(mensaje);
		return response.json();
	},
);

export const mensajesSlice = createSlice({
	name: 'mensajesReducer',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(getMensajes.fulfilled, (state, action: PayloadAction<Mensaje[]>) => {
			state.listaMensajes = [...action.payload];
			state.loading = false;
			state.error = '';
		});
		builder.addCase(getMensajes.pending, (state) => {
			state.loading = true;
			state.error = '';
		});
		builder.addCase(getMensajes.rejected, (state, action) => {
			state.loading = false;
			state.error = action.error.toString();
		});

		builder.addCase(addMensaje.fulfilled, (state, action) => {
			state.loading = true;
			state.error = '';
			if (action.payload.errors) {
				state.result = action.payload.errors.asunto;
			} else {
				state.result = action.payload[0].ok ?
					'Mensaje añadido correctamente' : 'Fallo al añadir mensaje';
			}
		});
		builder.addCase(addMensaje.pending, (state) => {
			state.loading = true;
			state.error = '';
		});
		builder.addCase(addMensaje.rejected, (state, action) => {
			state.loading = false;
			state.error = action.error.toString();
		});
	},
})

export const mensajesSelector = (state: RootState) => state.mensajesReducer;

export default mensajesSlice.reducer;
