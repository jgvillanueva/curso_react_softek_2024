import {configureStore} from "@reduxjs/toolkit";
import mensajesReducer from "./mensajesReducer";
import {TypedUseSelectorHook, useSelector} from 'react-redux';

export const store = configureStore({
	reducer: {
		mensajesReducer: mensajesReducer,
	}
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
