import mensajesReducer, {getMensajes, MensajesState} from "./mensajesReducer";


const initialState: MensajesState = {
	listaMensajes: [],
	loading: false,
	error: '',
	result: '',
}

const mensaje = {
	id: 1,
	asunto: 'hola',
	mensaje: 'nada interesante',
	user_id: 13,
}

describe('Mensajes reducer', () => {
	it('controla correctamente el caso pending', () => {
		const getAction = {
			type: getMensajes.pending.type,
		}
		const state = mensajesReducer(initialState, getAction);
		expect(state.loading).toBeTruthy();
		expect(state.listaMensajes.length).toEqual(0);
	});
	it('controla correctamente la carga de mensajes', () => {
		const getAction = {
			type: getMensajes.fulfilled.type,
			payload: [mensaje],
		}
		const state = mensajesReducer(initialState, getAction);
		expect(state.loading).toBeFalsy();
		expect(state.listaMensajes.length).toEqual(1);
		expect(state.listaMensajes[0].asunto).toEqual(mensaje.asunto);
	});
	it('controla correctamente errores', () => {
		const getAction = {
			type: getMensajes.rejected.type,
			error: 'Fake error',
		}
		const state = mensajesReducer(initialState, getAction);
		expect(state.loading).toBeFalsy();
		expect(state.listaMensajes.length).toEqual(0);
		expect(state.error).toEqual('Fake error');
	});
})
