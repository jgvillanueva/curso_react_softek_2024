import {FormEvent, useEffect, useRef, useState} from "react";
import {AppDispatch, useAppSelector} from "../redux/store";
import {addMensaje, mensajesSelector} from "../redux/mensajesReducer";
import {useDispatch} from "react-redux";

export function New() {
	const asunto = useRef<HTMLInputElement | null>(null);

	const mensajesState = useAppSelector(mensajesSelector);

	const [result, setResult] = useState<string>('');
	const [loading, setLoading] = useState<boolean>(false);
	const [error, setError] = useState<string>('');

	useEffect(() => {
		setResult(mensajesState.result);
		setLoading(mensajesState.loading);
		setError(mensajesState.error);
	}, [mensajesState]);

	const dispatch: AppDispatch = useDispatch();
	const handleClick = (event: FormEvent) => {
		event.preventDefault();
		if (asunto && asunto.current) {
			dispatch(addMensaje({
				asunto: asunto.current.value,
				mensaje: 'mensaje de prueba',
				user_id: '13',
			}))
		}
	}

	return(
		<div>
			<h1>Nuevo mensaje</h1>
			{loading ? <h2>... cargando</h2> : null}
			{error && error !== '' ? <h2>{error}</h2> : null}
			{result && result !== '' ? <h2>{result}</h2> : null}
			<form>
				<input ref={asunto} />
				<button onClick={handleClick}>Enviar</button>
			</form>
		</div>
	)
}
