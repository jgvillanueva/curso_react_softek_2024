import {useDispatch} from "react-redux";
import {getMensajes, mensajesSelector} from "../redux/mensajesReducer";
import {useEffect, useState} from "react";
import {AppDispatch, useAppSelector} from "../redux/store";
import {Mensaje} from "../services/data.service";

export function Lista() {
	const mensajesState = useAppSelector(mensajesSelector);

	const [mensajes, setMensajes] = useState<Mensaje[]>([]);
	const [loading, setLoading] = useState<boolean>(false);
	const [error, setError] = useState<string>('');


	useEffect(() => {
		setMensajes(mensajesState.listaMensajes);
		setLoading(mensajesState.loading);
		setError(mensajesState.error);
	}, [mensajesState]);

	const dispatch: AppDispatch = useDispatch();
	useEffect(() => {
		dispatch(getMensajes())
	}, [dispatch]);

	const renderItems = () => {
		return mensajes.map((mensaje: Mensaje) => {
			return <li key={mensaje.id} data-testid="mensaje">{mensaje.asunto}</li>
		})
	}

	return(
		<div data-testid="lista">
			<h1>Lista</h1>
			{loading ? <h2>... cargando</h2> : null}
			{error && error !== '' ? <h2>{error}</h2> : null}
			<ul>
				{renderItems()}
			</ul>
		</div>
	)
}
