import {fireEvent, render, screen, within} from '@testing-library/react';
import App from './App';
import {store} from "./redux/store";
import {Provider} from "react-redux";

describe('App component', () => {
  it('renders learn react link', () => {
    render(<Provider store={store}><App /></Provider>);
    const titleElement = screen.getByText(/Redux/i);
    expect(titleElement).toBeInTheDocument();
  });

  it('redux works', () => {
    render(<Provider store={store}><App /></Provider>);
    const texto = 'tests';
    const lista = screen.getByTestId('lista-component');
    expect(lista).toBeInTheDocument();
    const campo = screen.getByPlaceholderText('Add todo');
    expect(campo).toBeInTheDocument();
    campo.value = texto;
    const boton = screen.getByText(/Enviar/i);
    expect(boton).toBeInTheDocument();
    fireEvent.click(boton);
    const nuevoTodo = within(lista).getByText(texto);
    expect(nuevoTodo).toBeInTheDocument();
  });
});

