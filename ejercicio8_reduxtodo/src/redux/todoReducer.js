import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    todoList: [{id: 0, name: 'añadir dispatchers'}],
    lastIndex: 0,
};
export const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        add: (state, action) => {
            state.lastIndex ++;
            const todo = {
                id: state.lastIndex,
                name: action.payload,
            };
            const lista = [...state.todoList];
            lista.push(todo);
            state.todoList = lista;
        },
        remove: (state, action) => {
            const newLista = state.todoList
                .filter(todo => todo.id !== action.payload);
            state.todoList = [...newLista];
        }
    }
});

export const {add, remove} = todoSlice.actions;
export  default todoSlice.reducer;
