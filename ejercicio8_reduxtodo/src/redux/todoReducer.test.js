import todoReducer, {add, remove} from "./todoReducer";

describe('testing reducers', ()=>{
    it('test initial state', () => {
        expect(todoReducer(undefined, {type: undefined}).lastIndex).toEqual(0);
        expect(todoReducer(undefined, {type: undefined}).todoList.length).toEqual(1);
    });
    it('add todo', () => {
        const todoText = 'Crea tests';
        const reducer =
            todoReducer({
                lastIndex: 0,
                todoList: [],
            }, add(todoText));
        expect(reducer.lastIndex).toEqual(1);
        expect(reducer.todoList).toEqual([{
            id: 1,
            name: todoText,
        }]);
    });
    it('remove todo', () => {
        const todos = [
            {id: 5, name: 'todo inicial'},
            {id: 8, name: 'otro todo'},
        ]
        const reducer = todoReducer({
            lastIndex: 8,
            todoList: [...todos],
        }, remove(5));
        expect(reducer.lastIndex).toEqual(8);
        expect(reducer.todoList.length).toEqual(todos.length - 1);
        expect(reducer.todoList[0]).toEqual(todos[1]);
    });
})
