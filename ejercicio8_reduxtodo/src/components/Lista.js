import {useDispatch, useSelector} from "react-redux";
import styles from './lLsta.module.css';
import {remove} from "../redux/todoReducer";
import {useCallback, useMemo} from "react";
import Todo from "./Todo";

export default function Lista() {
    const todoList = useSelector((state) => {
        return state.todos.todoList;
    });

    const dispatch = useDispatch();

    const hadleClick = useCallback(
        (id) => {
            dispatch(remove(id));
        }, [dispatch]
    );

    const todos = useMemo(() => {
        return todoList.map(todo => {
            return <Todo key={todo.id} todo={todo} hadleClick={hadleClick} />
        });
    }, [todoList]);

    return (
        <div className={styles.lista}  data-testid="lista-component">
            <h1>Lista</h1>
            <ul>
                {todos}
            </ul>
        </div>
    );
}
