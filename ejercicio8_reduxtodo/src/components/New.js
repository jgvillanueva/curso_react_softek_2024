import {useDispatch} from "react-redux";
import {add} from "../redux/todoReducer";
import React, {useRef} from "react";

const New = () => {
    const dispatch = useDispatch();
    const nombre = useRef();

    const handleClick = (event) => {
        event.preventDefault();
        dispatch(add(nombre.current.value));
    }

    return (
        <div >
            <h1>New</h1>
            <form>
                <input ref={nombre} placeholder="Add todo" />
                <button onClick={handleClick}>Enviar</button>
            </form>
        </div>
    );
}

export default React.memo(New)
