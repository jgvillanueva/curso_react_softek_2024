
import './App.css';
import Lista from "./components/Lista";
import New from "./components/New";

function App() {
    return (
        <div className="App">
            <h1>Redux</h1>
            <New />
            <Lista />
        </div>
    );
}

export default App;
