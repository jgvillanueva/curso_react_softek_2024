
export default function Layout({children}: Readonly<{children: React.ReactNode}>) {
	return(
		<div>
			<h1>Apartado usuarios</h1>
			{children}
		</div>
	)
}
