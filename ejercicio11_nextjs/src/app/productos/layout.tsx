'use client';

import {ProductosProvider} from "@/utils/productosContext";
import {productos} from "@/utils/productos.service";

export default function Layout({children}: Readonly<{children: React.ReactNode}>) {
	return(
		<div>
			<h1>Apartado productos</h1>
			<ProductosProvider value={productos}>
				{children}
			</ProductosProvider>

		</div>
	)
}
