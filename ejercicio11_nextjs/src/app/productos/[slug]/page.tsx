'use client';

import {Producto} from "@/utils/productos.service";
import {useContext} from "react";
import {ProductosContext} from "@/utils/productosContext";

export default function Page({params}: {params: {slug: string}}) {
	const productos: Producto[] = useContext(ProductosContext) as Producto[];

	const {slug} = params;

	const producto = productos.find(_producto =>
		Number(slug) === _producto.id
	);

	return(
		<div>
			<h2>Lista de usuarios usuarios</h2>
			{
				producto ?
					<div>
						<h2>{producto.nombre}</h2>
						<h2>{producto.precio}</h2>
					</div>
					:
					<div>No se encuentra el producto</div>
			}
		</div>
	)
}
