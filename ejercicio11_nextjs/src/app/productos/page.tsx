'use client';

import {useContext} from "react";
import {ProductosContext} from "@/utils/productosContext";
import {Producto} from "@/utils/productos.service";

export default function Page() {

	const productos: Producto[] = useContext(ProductosContext) as Producto[];

	const renderProductos = () => {
		return productos.map(producto => {
			return <li key={producto.id}>
				<h3>{producto.nombre}</h3>
				<h4>{producto.precio}</h4>
			</li>
		})
	}

	return(
		<div>
			<h2>Home del apartado productos</h2>
			{renderProductos()}
		</div>
	)
}
