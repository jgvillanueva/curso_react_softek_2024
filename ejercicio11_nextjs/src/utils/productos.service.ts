export interface Producto {
	id: number,
	nombre: string,
	precio: number,
}

export const  productos: Producto[] = [
	{
		id: 1,
		nombre: 'camisa',
		precio: 30,
	},
	{
		id: 2,
		nombre: 'zapato',
		precio: 40,
	},
	{
		id: 3,
		nombre: 'calcetín',
		precio: 10,
	},
]
