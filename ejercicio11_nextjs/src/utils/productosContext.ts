import {createContext} from "react";

export const  ProductosContext = createContext({});

export const ProductosProvider = ProductosContext.Provider;
