import {configureStore} from "@reduxjs/toolkit";
import appStatusReducer from "./appStatusReducer";
import {TypedUseSelectorHook, useSelector} from "react-redux";
import {mensajesAPI} from "./apiStatusReducer";
import {setupListeners} from "@reduxjs/toolkit/query";

export const store = configureStore({
	reducer: {
		appStatusReducer: appStatusReducer,
		[mensajesAPI.reducerPath]: mensajesAPI.reducer,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({}).concat(mensajesAPI.middleware),
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
