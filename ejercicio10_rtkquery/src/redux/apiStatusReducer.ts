
import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";

export interface Mensaje {
	id?: string;
	asunto: string;
	mensaje: string;
	user_id?: string;
}

export const mensajesAPI = createApi({
	reducerPath: 'mensajesAPI',
	tagTypes: ['mensajes'],
	baseQuery: fetchBaseQuery({
		baseUrl: 'http://dev.contanimacion.com/api_tablon/api/',
	}),
	endpoints: (builder) => ({
		getMensajes: builder.query<Mensaje[], void>({
			query: () => 'mensajes',
			providesTags: ['mensajes'],
		}),
		newMensaje: builder.mutation({
			query: (args) => ({
				url: 'mensajes/add',
				method: 'POST',
				body: args,
			}),
			invalidatesTags: ['mensajes'],
		}),
	})
});


export const {useGetMensajesQuery, useNewMensajeMutation} = mensajesAPI;
