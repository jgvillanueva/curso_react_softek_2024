import {useNewMensajeMutation} from "../redux/apiStatusReducer";
import {FormEvent, useEffect, useRef, useState} from "react";
import {FetchBaseQueryError} from "@reduxjs/toolkit/query";

export default function New() {
	const [newMensaje, requestStatus] = useNewMensajeMutation();

	const [isLoading, setIsLoading] = useState(false);
	const [isError, setIsError] = useState(false);
	const [error, setError] = useState<FetchBaseQueryError>();

	const asunto = useRef<HTMLInputElement | null>(null);
	const handleClick = async (event: FormEvent) => {
		event.preventDefault();
		if (asunto && asunto.current) {
			const response  = await newMensaje({
				asunto: asunto.current.value,
				mensaje: 'mensaje de prueba',
				user_id: 13,
			});
			console.log('RequestStatus: ', requestStatus);
		}
	}

	useEffect(() => {
		setIsLoading(requestStatus.isLoading);
		setIsError(requestStatus.isError);
		setError(requestStatus.error as FetchBaseQueryError);
	}, [requestStatus]);

	return (
		<div>
			<h1>New</h1>
			{isLoading? <h2>... cargando datos</h2> : null}
			{isError? <h2>Error cargando datos: {error?.status}</h2> : null}
			<form>
				<input ref={asunto} />
				<button onClick={handleClick}>Enviar</button>
			</form>
		</div>
	)
}
