import {Mensaje, useGetMensajesQuery} from "../redux/apiStatusReducer";
import {useMemo} from "react";
import {FetchBaseQueryError} from "@reduxjs/toolkit/query";

export default function Lista() {
	const query = useGetMensajesQuery();
	const data = query.data;
	const isLoading = query.isLoading;
	const isError = query.isError;
	const error: FetchBaseQueryError = query.error as FetchBaseQueryError;

	const items = useMemo(() => {
		if (!data) return null;

		return data.map((mensaje: Mensaje) => {
			return <li key={mensaje.id}>{mensaje.asunto}</li>
		});
	}, [data])

	return (
		<div>
			<h1>Lista</h1>
			{isLoading? <h2>... cargando datos</h2> : null}
			{isError? <h2>Error cargando datos: {error.status}</h2> : null}
			{items}
		</div>
	)
}
