import React from 'react';
import logo from './logo.svg';
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "./redux/store";
import {APP_VIEWS, changeView} from "./redux/appStatusReducer";
import Lista from "./components/Lista";
import New from "./components/New";

function App() {
  const view = useSelector((state: RootState) => {
    return state.appStatusReducer.view;
  });

  const renderView = () => {
    switch (view) {
      case APP_VIEWS.lista:
        return <Lista />;
      case APP_VIEWS.new:
        return <New />;
      default:
        return <div><h1>Home</h1></div>
    }
  }

  const dispatch: AppDispatch = useDispatch();

  const cambiaVista = (value: string) => {
    dispatch(changeView({view: value}));
  }


  return (
    <div className="App">
      <nav>
        <ul>
          <li onClick={() => cambiaVista(APP_VIEWS.lista)}>Lista</li>
          <li onClick={() => cambiaVista(APP_VIEWS.new)}>New</li>
        </ul>
      </nav>
      {renderView()}
    </div>
  );
}

export default App;
