import {FormEvent, useEffect, useId, useState} from "react";
import styles from './DebounceInput.module.css';

type DebounceProps = {
	label: string,
	inputType?: string,
	handleInputChange: Function,
	name: string,
	debounceTime?: number,
}
export function DebounceInput(props: DebounceProps) {
	const [inputValue, setInputValue] = useState('');
	const [debouncededInputValue, setdebouncededInputValue] = useState('');

	const {debounceTime, label, inputType, handleInputChange, name} = props;

	const id = useId();
	const handleChangeInterno = (event: FormEvent) => {
		const target: HTMLInputElement = event.target as HTMLInputElement;
		setInputValue(target.value);
	}

	useEffect(() => {
		const timeoutId = setTimeout(() => {
			setdebouncededInputValue(inputValue);
		}, debounceTime);
		return () => { clearTimeout(timeoutId)}
	}, [inputValue, debounceTime]);

	useEffect(() => {
		handleInputChange(debouncededInputValue);
	}, [debouncededInputValue, handleInputChange]);

	return (
		<div className={styles.inputContainer}>
			<label  className={styles.label}>{label}</label>
			<input
				className={styles.input}
				id={id}
				type={inputType}
				name={name}
				value={inputValue}
				onChange={handleChangeInterno}
			/>
		</div>
	)
}

DebounceInput.defaultProps = {
	debounceTime: 500,
	inputType: 'text',
}
