import {loadData, Mensaje} from "../../services/dataLoader";
const loader = loadData();

function ListaMensajes() {
	const data = loader.read();

	const renderMensajes = () => {
		return data.map((mensaje: Mensaje) => {
			return <li key={mensaje.id}>{mensaje.asunto}</li>
		});
	}

	return (
		<div>
			<h1>Lista</h1>
			<ul>
				{renderMensajes()}
			</ul>
		</div>

	)
}
export default ListaMensajes;
