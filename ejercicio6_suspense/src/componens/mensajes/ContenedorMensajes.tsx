import ListaMensajes from "./ListaMensajes";
import {Suspense} from "react";

function ContenedorMensajes() {
	return (
		<div>
			<h2>Mensajes</h2>
			<Suspense fallback={<h1>... cargando datos</h1>}>
				<ListaMensajes/>
			</Suspense>
		</div>

	)
}

export default ContenedorMensajes;
