

// @ts-ignore
import {Mensaje} from "./dataLoader";

export default function wrapPromise(promise: Promise<Mensaje[]>) {
	let status = 'pending';
	let response: Mensaje[] | undefined;
	const  suspender = promise.then(
		res => {
			status = 'success';
			response = res;
		},
		err => {
			status = 'error';
			response = err;
		}
	);

	// @ts-ignore
	const handle = {
		pending: () => {
			throw suspender;
		},
		error: () => {
			throw response;
		},
		default: () => response
	}

	const read = () => {
		// @ts-ignore
		const result = handle[status] ? handle[status]() : handle.default();
		return result;
	}

	return {read};
}
