import wrapPromise from "./wrapPromise";

export type Mensaje = {
	id?: string,
	asunto: string,
	mensaje: string,
}

export const  loadData = () => {
	const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
	const promise = fetch(url, {method: 'GET'})
		.then(result => result.json())
		.then(result => result)
	return wrapPromise(promise);
}

