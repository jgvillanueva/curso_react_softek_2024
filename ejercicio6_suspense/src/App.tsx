import React, {Suspense} from 'react';
import './App.css';
import ContenedorMensajes from "./componens/mensajes/ContenedorMensajes";
import {DebounceInput} from "./componens/debounceInput/DebounceInput";

function App() {
    return (
        <div className="App">
            <Suspense>
                <DebounceInput
                    label="Campo Debounce"
                    name="asunto"
                    handleInputChange={(value: string) => {console.log(value);}}
                />
                <ContenedorMensajes />
            </Suspense>
        </div>
    );
}

export default App;
