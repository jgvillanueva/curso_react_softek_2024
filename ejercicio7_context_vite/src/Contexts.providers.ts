import {createContext} from "react";

export const StringContext = createContext({});
export const StringProvider = StringContext.Provider;

export const DataServiceContext = createContext({});
export const DataServiceProvider = DataServiceContext.Provider;
