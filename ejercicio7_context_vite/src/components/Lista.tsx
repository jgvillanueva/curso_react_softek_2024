import {useContext, useEffect, useState} from "react";
import {DataServiceContext, StringContext} from "../Contexts.providers.ts";
import {DataServiceType, Mensaje} from "../services/Data.service.ts";

export default function Lista() {
	const strings: {[key: string]: string} = useContext(StringContext);

	const dataService: DataServiceType = useContext(DataServiceContext) as DataServiceType;

	const [mensajes, setMensajes] = useState<Mensaje[]>()
	useEffect(() => {
		const getData = async () => {
			const data = await dataService.getAll();
			setMensajes(data);
		};
		getData();
	}, [dataService, setMensajes]);

	const renderMensajes = () => {
		return mensajes?.map((mensaje) => {
			return <li key={mensaje.id}>{mensaje.asunto}</li>
		})
	}

	return (
		<div>
			<h1>{strings.tituloLista}</h1>
			{renderMensajes()}
		</div>
	)
}
