import {useContext} from "react";
import {StringContext} from "../Contexts.providers.ts";

export default function New() {
	const strings: {[key: string]: string} = useContext(StringContext);

	return (
		<div>
			<h1>{strings.tituloNew}</h1>
		</div>
	)
}
