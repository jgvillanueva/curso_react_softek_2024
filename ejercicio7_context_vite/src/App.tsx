
import './App.css'
import New from "./components/New.tsx";
import Lista from "./components/Lista.tsx";
import {DataServiceProvider, StringProvider} from "./Contexts.providers.ts";
import dataService from "./services/Data.service.ts";

function App() {
    const strings = {
        tituloLista: 'Lista de mensajes',
        tituloNew: 'nuevo mensaje',
    }

  return (
    <>
        <StringProvider value={strings}>
            <New />
            <DataServiceProvider value={dataService}>
                <Lista />
            </DataServiceProvider>
        </StringProvider>
    </>
  )
}

export default App
