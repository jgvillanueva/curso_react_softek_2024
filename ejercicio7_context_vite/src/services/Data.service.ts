export type Mensaje = {
	asunto: string,
	mensaje: string,
	id: string,
}

export type DataServiceType = {
	getAll: () => Promise<Mensaje[]>,
}

class DataService {
	async getAll() {
		return new Promise((resolve) => {
			const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
			fetch(url, {method: 'GET'})
				.then(result => result.json())
				.then(result => resolve(result));
		})

	}
}
const dataService = new DataService();
export default dataService;
