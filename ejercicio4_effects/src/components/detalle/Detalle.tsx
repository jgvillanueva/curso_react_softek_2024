import {FC, ReactElement, useEffect, useState} from "react";
import styles from './Detalle.module.scss';
import {useLocation} from "react-router-dom";
import {Mensaje} from "../lista/List";

type IProps = {
}

const Detalle: FC<IProps> = (props: IProps): ReactElement => {
	const initialMensaje: Mensaje = {
		id: 0,
		asunto: '',
		mensaje: '',
	}
	const [mensaje, setMensaje] = useState<Mensaje>(initialMensaje);
	const location = useLocation();

	const stateMensaje = location.state?.stateMensaje;

	useEffect(() => {
		setMensaje(stateMensaje);
	}, [stateMensaje]);

	if (!mensaje) {
		return (<h1>No hay mensaje</h1>)
	}

	return (
		<div className={styles.detalle}>
			<h2>{mensaje.asunto}</h2>
			<div> { mensaje.mensaje } </div>
		</div>
	);
}

export default Detalle;
