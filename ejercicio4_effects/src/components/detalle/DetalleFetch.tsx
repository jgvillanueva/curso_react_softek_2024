import {FC, ReactElement, useEffect, useState} from "react";
import styles from './Detalle.module.scss';
import {useParams} from "react-router-dom";
import {Mensaje} from "../lista/List";

type IProps = {
}

const DetalleFetch: FC<IProps> = (props: IProps): ReactElement => {
	const url = process.env.REACT_APP_API_URL;

	const initialMensaje: Mensaje = {
		id: 0,
		asunto: '',
		mensaje: '',
	}
	const [mensaje, setMensaje] = useState<Mensaje>(initialMensaje);

	const {id} = useParams();

	useEffect(() => {
		const getData = async () => {
			const response = await fetch(url + '/get/' + id, {method: 'GET'});
			const data = await response.json();
			setMensaje(data);
		}

		getData()
			.catch(console.error);
	}, [id, url]);

	return (
		<div className={styles.detalle}>
			<h2>{mensaje.asunto}</h2>
			<div> { mensaje.mensaje } </div>
		</div>
	);
}

export default DetalleFetch;
