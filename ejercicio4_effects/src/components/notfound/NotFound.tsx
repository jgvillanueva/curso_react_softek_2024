import {FC, ReactElement} from "react";

type IProps = {
}

const NotFound: FC<IProps> = (props: IProps): ReactElement => {
	return (
		<h1 >
			La ruta no existe
		</h1>
	);
}

export default NotFound;
