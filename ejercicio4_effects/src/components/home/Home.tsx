import {FC, ReactElement} from "react";

type IProps = {
}

const Home: FC<IProps> = (props: IProps): ReactElement => {
	return (
		<h1 >
			React routes
		</h1>
	);
}

export default Home;
