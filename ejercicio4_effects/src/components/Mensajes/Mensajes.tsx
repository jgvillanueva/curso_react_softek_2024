import {FC, ReactElement, useEffect, useState} from "react";
import {useLoaderData} from "react-router-dom";
import {Mensaje} from "../lista/List";
import Item from "../item/Item";

type IProps = {
}

const Mensajes: FC<IProps> = (props: IProps): ReactElement => {
	const [mensajes, setMensajes] = useState<Mensaje[]>([])
	const loaderData: Mensaje[] = useLoaderData() as Mensaje[];

	useEffect(() => {
		setMensajes(loaderData);
	}, [loaderData]);

	const renderMensajes = () => {
		return mensajes?.map(mensaje=> {
			return <Item key={'item_' + mensaje.id} mensaje={mensaje} />
		});
	}

	return (
		<div>
			<h1>Mensajes</h1>
			{renderMensajes()}
		</div>
	);
}

export default Mensajes;
