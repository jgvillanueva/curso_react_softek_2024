import {FC, ReactElement} from "react";
import {Link, Outlet} from "react-router-dom";

type IProps = {
}

const Usuario: FC<IProps> = (props: IProps): ReactElement => {
	return (
		<div data-testid="usuario-component">
			<h1>Usuario </h1>
			<nav>
				<ul>
					<li><Link to="preferencias" >Preferencias</Link></li>
					<li><Link to="cuenta" >Cuenta</Link></li>
				</ul>
			</nav>

			<Outlet />
		</div>
	);
}

export default Usuario;
