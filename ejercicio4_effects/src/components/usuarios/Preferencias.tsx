import {FC, ReactElement} from "react";
import {useNavigate} from "react-router-dom";

type IProps = {
}

const Preferencias: FC<IProps> = (props: IProps): ReactElement => {
	const navigate = useNavigate();
	const back = () => {
		navigate(-1);
	}
	return (
		<div>
			<h2>
				Preferencias
			</h2>
			<button onClick={back}>Volver</button>
		</div>

	);
}

export default Preferencias;
