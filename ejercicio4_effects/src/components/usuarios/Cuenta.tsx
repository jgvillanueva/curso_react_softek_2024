import {FC, ReactElement} from "react";
import {Link} from "react-router-dom";

type IProps = {
}

const Cuenta: FC<IProps> = (props: IProps): ReactElement => {
	return (
		<div>
			<h2>
				Cuenta
			</h2>
			<Link to=".." >Volver</Link>
		</div>

	);
}

export default Cuenta;
