import {FC, ReactElement} from "react";
import {Mensaje} from "../lista/List";
import styles from './Item.module.scss';
import {Link, NavigateOptions, useNavigate} from "react-router-dom";

type IProps = {
	mensaje: Mensaje,
}

const Item: FC<IProps> = (props: IProps): ReactElement => {
	const navigate = useNavigate();
	const goDetalle = () => {
		const options: NavigateOptions = {
			state: {
				stateMensaje: props.mensaje,
			}
		}
		navigate('detalle', options);
	}

	const goDetalleFetch = () => {
		navigate('./' + props.mensaje.id);
	}

	return (
		<li className={styles.item} data-testid="item">
			<h3>{props.mensaje.asunto}</h3>
			<div>
				<p>{props.mensaje.mensaje}</p>
			</div>

			<div>
				<Link
					to={"./" + props.mensaje.id}
				>Ver detalle fetch</Link>
				<button onClick={goDetalleFetch}>Ver detalle fetch</button>
			</div>

			<div>
				<Link
					to="detalle"
					state={{stateMensaje: props.mensaje}}
				>Ver detalle state</Link>
				<button onClick={goDetalle}>Ver detalle state</button>
			</div>
		</li>
	);
}

export default Item;
