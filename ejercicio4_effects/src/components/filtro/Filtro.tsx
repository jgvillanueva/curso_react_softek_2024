import {FC, ReactElement, useEffect, useState} from "react";
import Item from "../item/Item";
import styles from '../lista/List.module.scss';
import {Mensaje} from "../lista/List";
import {useLocation, useSearchParams} from "react-router-dom";

type IProps = {
}

const Filtro: FC<IProps> = (props: IProps): ReactElement => {
	const [mensajes, setMensajes] = useState<Mensaje[]>([]);
	const [filtro, setFiltro] = useState<string | null>();

	const [searchParams] = useSearchParams();
	const textoFiltro = searchParams.get('textoFiltro');

	useEffect(() => {
		setFiltro(textoFiltro);
	}, [textoFiltro]);

	const location = useLocation();
	const {mensajesRaw} = location.state;

	useEffect(() => {
		if (mensajesRaw) {
			setMensajes(mensajesRaw);
		}
	}, [mensajesRaw]);

	const renderMensajes = () => {
		if(!mensajes)
			return null;

		const mensajesFiltrados = filtro ?
			mensajes.filter(mensaje => mensaje.asunto.toLowerCase().includes(filtro.toLowerCase()))
			:
			mensajes
		;

		return mensajesFiltrados.map(mensaje=> {
			return <Item key={'item_' + mensaje.id} mensaje={mensaje} />
		});
	}

	return (
		<div className={styles.lista}>
			<h1>Filtro</h1>
			<ul>
				{ renderMensajes() }
			</ul>
		</div>
	);
}

export default Filtro;
