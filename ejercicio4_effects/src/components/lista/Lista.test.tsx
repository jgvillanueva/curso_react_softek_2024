import {render, screen, waitFor} from "@testing-library/react";
import List, {Mensaje} from "./List";
import {BrowserRouter} from "react-router-dom";
import fetchMock, {MockResponse} from "fetch-mock";

/*
fetch-mock puede dar problemas con npm, pero con esta versión funciona:
	"fetch-mock": "^9.3.2",
    "node-fetch": "^2.6.1"
 */

describe('Lista component', () => {
	it('should render lista de mensajes', async () => {
		render(<BrowserRouter><List apiUrl='https://dev.contanimacion.com/api_tablon/api/mensajes' /></BrowserRouter>);
		const primerMensaje = await screen.findByText('este es el asunto');
		expect(primerMensaje).toBeInTheDocument();
	});
})

const mockUrl = 'https://testmockMensajes/mensajes';

const mensajes: Mensaje[] = [
	{
		"id": 50,
		"asunto": "asunto",
		"mensaje": "Esto es una prueba",
		"user_id": 13,
		"created_at": "2020-08-04 15:48:27"
	},
	{
		"id": 51,
		"asunto": "asunto",
		"mensaje": "Nueva prueba",
		"user_id": 13,
		"created_at": "2020-08-04 15:49:21"
	},
	{
		"id": 52,
		"asunto": "asunto",
		"mensaje": "Hola",
		"user_id": 13,
		"created_at": "2020-08-04 15:56:57"
	},
	{
		"id": 53,
		"asunto": "asunto",
		"mensaje": "c\u00f3mo est\u00e1s ?",
		"user_id": 13,
		"created_at": "2020-08-04 15:57:11"
	},
];

describe('Mockeando mensajes', () => {
	beforeEach(() => {
		fetchMock.get(mockUrl, mensajes);
	});

	afterEach(() => {
		fetchMock.reset();
	})

	it('renderiza la misma cantidad de mensajes', async () => {
		render(<BrowserRouter>
			<List apiUrl={mockUrl} />
		</BrowserRouter>)
		const mensajesCargados = await screen.findAllByTestId('item');
		expect(mensajesCargados.length).toEqual(mensajes.length);

		await waitFor(() => {
			const error = screen.queryByTestId('error');
			expect(error).not.toBeInTheDocument();
		})
	});

	it('renderiza error si hay un problema', async () => {
		fetchMock.catch(new Response('not found', {status: 404, statusText: 'not found'}));
		render(<BrowserRouter>
			<List apiUrl='https://testmockMensajes/mensajes/noexisten' />
		</BrowserRouter>)
		const error = await screen.findByTestId('errorFetch');
		expect(error).toBeInTheDocument();
		const errorByText = await screen.findByText(/not found/i);
		expect(errorByText).toBeInTheDocument();
	});
})

describe('no se lanza dos veces la petición', () => {
	it('no crea error', async () => {
		const response: MockResponse = {
			body: mensajes,
		}
		fetchMock.once(mockUrl, response);
		render(<BrowserRouter>
			<List apiUrl={mockUrl} />
		</BrowserRouter>)
		const mensajesCargados = await screen.findAllByTestId('item');
		expect(mensajesCargados.length).toEqual(mensajes.length);
	})
})

