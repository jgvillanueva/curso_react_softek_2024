import {FC, ReactElement, useEffect, useState} from "react";
import Item from "../item/Item";
import styles from './List.module.scss';
import {createSearchParams, NavigateOptions, useNavigate} from "react-router-dom";

type IProps = {
	apiUrl: string | undefined,
}

export interface Mensaje {
	id: number,
	asunto: string,
	mensaje: string,
	user_id?: number,
	created_at?: string,
}

const List: FC<IProps> = (props: IProps): ReactElement => {
	const [mensajes, setMensajes] = useState<Mensaje[]>([]);
	const [error, setError] = useState<string | null>();

	useEffect(() => {
		if (props.apiUrl) {
			fetch(props.apiUrl, {method: 'GET'})
				.then(res => res.json())
				.then((response) => {
					console.log(response);
					if (response.ok === false) {
						setError(response.statusText);
					} else  {
						setMensajes(response as Mensaje[]);
					}
				})
				.catch(e => {
					setError(e.statusText);
				})
		}
	}, [props.apiUrl]);

	const renderMensajes = () => {
		return mensajes?.map(mensaje=> {
			return <Item key={'item_' + mensaje.id} mensaje={mensaje} />
		});
	}

	const navigate = useNavigate();
	const filtrar = () => {
		const options: NavigateOptions = {
			state: {mensajesRaw: mensajes}
		}
		navigate({
			pathname: 'filtro',
			search: createSearchParams(
				{
					textoFiltro: 'prueba',
				}
			).toString(),
		}, options);
	}

	const getError = () => {
		if (!error)
			return null;
		return <div data-testid="errorFetch">Error: {error}</div>
	}

	return (
		<div className={styles.lista}>
			<h1>Lista</h1>
			{ getError() }

			<div>
				<h2>Filtro:</h2>
				<button onClick={filtrar}>Filtrar</button>
			</div>
			<ul>
				{ renderMensajes() }
			</ul>
		</div>
	);
}

export default List;
