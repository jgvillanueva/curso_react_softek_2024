import React from 'react';
import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import App from './App';
import {BrowserRouter} from "react-router-dom";

describe('App component', () => {
  beforeEach(() => {
    render(<BrowserRouter><App /></BrowserRouter>);
  })

  it('render app', () => {
    const primeraPágina = screen.getByText(/React routes/i);
    expect(primeraPágina).toBeInTheDocument();
  });

  it('load usuarios', () => {
    const userLink = screen.getByText(/Usuario/i);
    expect(userLink).toBeInTheDocument();
    fireEvent.click(userLink);
    const vistaUusuario = screen.getByTestId('usuario-component');
    expect(vistaUusuario).toBeInTheDocument();
  });

  it('load mensajes', async () => {
    const listaLink = screen.getByText('Lista');
    expect(listaLink).toBeInTheDocument();
    fireEvent.click(listaLink);
    await waitFor(() => {
      const items = screen.queryAllByTestId('item');
      expect(items[0]).toBeInTheDocument();
    })
  });
})
