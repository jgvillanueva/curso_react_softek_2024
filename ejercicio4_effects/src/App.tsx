import React from 'react';
import './App.css';
import List from "./components/lista/List";
import Detalle from "./components/detalle/Detalle";
import {createBrowserRouter, Link, Route, RouterProvider, Routes, useNavigate} from "react-router-dom";
import Home from "./components/home/Home";
import Usuario from "./components/usuarios/Usuario";
import Preferencias from "./components/usuarios/Preferencias";
import Cuenta from "./components/usuarios/Cuenta";
import NotFound from "./components/notfound/NotFound";
import DetalleFetch from "./components/detalle/DetalleFetch";
import Filtro from "./components/filtro/Filtro";
import Mensajes from "./components/Mensajes/Mensajes";

function App() {
    const apiUrl = process.env.REACT_APP_API_URL;

    const navigate = useNavigate();
/*
    const router = createBrowserRouter([
        {
            path: '/',
            element: <Home />,
        },
        {
            path: '/mensajes',
            element: <Mensajes />,
            loader: async () => {
                return await fetch('https://dev.contanimacion.com/api_tablon/api/mensajes')
                    .then(response => response.json())
            }
        },
    ]);
*/
    return (
        <div className="App">
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="lista">Lista</Link>
                    </li>
                    <li>
                        <Link to="lista/detalle">Detalle</Link>
                    </li>
                    <li>
                        <Link to="user">Usuario</Link>
                    </li>
                </ul>
                <button onClick={() => {
                    navigate('detalle');
                }}>Detalle
                </button>
            </nav>

            <Routes>
                <Route path="/">
                    <Route index element={<Home/>}/>
                    <Route path="lista">
                        <Route path="" element={<List apiUrl={apiUrl}/>}/>
                        <Route path="detalle" element={<Detalle/>}/>
                        <Route path=":id" element={<DetalleFetch/>}/>
                        <Route path="filtro" element={<Filtro/>}/>
                    </Route>
                    <Route path="user" element={<Usuario/>}>
                        <Route path="preferencias" element={<Preferencias/>}/>
                        <Route path="cuenta" element={<Cuenta/>}/>
                    </Route>
                    <Route path="*" element={<NotFound/>}/>
                </Route>
            </Routes>
            {/*<RouterProvider router={router}></RouterProvider>*/}
        </div>
    );
}

export default App;
