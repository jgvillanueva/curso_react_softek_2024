import { render, screen } from '@testing-library/react';
import App from './App';

it('renders contador', () => {
  render(<App />);
  const tituloElement = screen.getByText(/Hola mundo/i);
  expect(tituloElement).toBeInTheDocument();
});
