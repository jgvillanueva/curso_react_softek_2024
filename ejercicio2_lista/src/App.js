
import './App.css';
import Contador from "./components/Contador/Contador";
import List from "./components/Lista/Lista";

function App() {
    const  usuarios = [
        {nombre: 'Jorge', edad: 73},
        {nombre: 'Juan', edad: 32},
        {nombre: 'Ana', edad: 43},
        {nombre: 'Carlos', edad: 12},
    ];
  return (
    <div className="App">
      <header className="App-header">
        <Contador titulo="Hola mundo" />
          <List data={usuarios} />
      </header>
    </div>
  );
}

export default App;
