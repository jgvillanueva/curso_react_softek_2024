import React from "react";
class Contador extends React.Component {
    interval;
    constructor(props) {
        super(props);
        this.state = {
            valor: 0,
        }

        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            this.setState({
                valor: this.state.valor + 1,
            });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleClick() {
        this.setState({
            valor: 0,
        })
    }

    render() {
        return (
            <div>
                <h2>{this.props.titulo}</h2>
                <div>{this.state.valor}</div>
                <button onClick={this.handleClick} >Reset</button>
            </div>
        )
    }
}

export default Contador;
