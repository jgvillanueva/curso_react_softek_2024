import {Item} from "../Item/Item";

export default function List(props) {
    const getItems = () => {
        return props.data.map((usuario, index) => {
            return <Item key={index} usuario={usuario}>
                <h3>{index}</h3>
            </Item>
        });
    }

    return (
        <div className="list">
            <ul>
                { getItems() }
            </ul>
        </div>
    )
}
