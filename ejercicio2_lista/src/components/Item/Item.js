export function Item(props) {
    return (
        <>
            <li>
                {props.usuario.nombre}: {props.usuario.edad}
                {props.children}
            </li>
        </>
    )
}
