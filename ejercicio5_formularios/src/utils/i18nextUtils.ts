import i18n from "i18next";
import {initReactI18next} from "react-i18next";

export const INITIAL_TRANSLATION = {
	es: {
		translation: {
			"title": "Controlado",
			"label_asunto": "Asunto",
			"label_mensaje": "Mensaje",
			"label_user_id": "Usuario",
			"button_enviar": "Enviar",
		}
	},
	en: {
		translation: {
			"title": "Controlled",
			"label_asunto": "Subject",
			"label_mensaje": "Message",
			"label_user_id": "User",
			"button_enviar": "Send",
		}
	},
}

i18n
	.use(initReactI18next)
	.init({
		lng: 'en',
		fallbackLng: 'en',
		debug: true,

		interpolation: {
			escapeValue: false, // not needed for react as it escapes by default
		},

		resources: INITIAL_TRANSLATION,
	});
export default i18n;
