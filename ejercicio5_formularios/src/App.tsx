import React, {useState} from 'react';
import './App.css';
import Toggle from "./componentes/toggle/Toggle";
import Controlled from "./componentes/Controlled/Controlled";
import UnControlled from "./componentes/UnControlled/UnControlled";
import FormikForm from "./componentes/formikForm/FormikForm";
import i18n from "i18next";

function App() {
    const [useFormik, setUseFormik] = useState<boolean>(false);
    const [controlled, setControlled] = useState<boolean>(true);
    const handleToggle = (value: boolean) => {
        setControlled(value);
    }

    const handleFormik = (value: boolean) => {
        setUseFormik(value);
    }

    const cambiaIdioma = async () => {
        await i18n.changeLanguage(i18n.language === 'en' ? 'es': 'en');
    }

    return (
        <div className="App">
            <h1>Formularios</h1>
            <button onClick={cambiaIdioma}>Cambia idiioma</button>
            <Toggle
                default={false}
                label="Usar formik"
                disabled={false}
                opcion1="Formik"
                opcion2="react Forms"
                handleToggle={handleFormik}
            />

            <Toggle
                default={true}
                label="Tipo de formulario"
                disabled={false}
                opcion1="Controlled"
                opcion2="UnControlled"
                handleToggle={handleToggle}
            />


            {
                useFormik ?
                    <FormikForm />
                    :
                    controlled ?
                        <Controlled />
                        :
                        <UnControlled />
            }
        </div>
    );
}

export default App;
