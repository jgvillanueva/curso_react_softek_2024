import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import {I18nextProvider} from "react-i18next";
import i18n, {INITIAL_TRANSLATION} from './utils/i18nextUtils'

describe('App component', () => {
  it('renders learn react link', () => {
    render(<I18nextProvider i18n={i18n}><App /></I18nextProvider>);
    const title = screen.getByText(/Formularios/i);
    expect(title).toBeInTheDocument();
  });

  it('test translations', async () => {
    render(<I18nextProvider i18n={i18n}><App /></I18nextProvider>);
    const title = screen.getByText(INITIAL_TRANSLATION.en.translation.title);
    expect(title).toBeInTheDocument();
  });
})

