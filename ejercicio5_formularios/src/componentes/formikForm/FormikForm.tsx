import styles from '../Form.module.scss';
import {useFormik} from "formik";

type IProps = {
}

interface Mensaje {
	asunto: string,
	mensaje: string,
}
interface Errores {
	asunto?: string,
	mensaje?: string,
}

function FormikForm (props: IProps) {
	const validate = (values: Mensaje): Errores => {
		const errores: Errores = {};
		if (!values.asunto) {
			errores.asunto = 'Asunto required';
		} else if (values.asunto.length < 5) {
			errores.asunto = 'El asunto debe tener más de 5 caracteres';
		}
		if (!values.mensaje) {
			errores.mensaje = 'Mensaje required';
		} else if (values.mensaje.length < 15) {
			errores.mensaje = 'El mensaje debe tener más de 15 caracteres';
		}
		return errores;
	}

	const formik = useFormik({
		initialValues: {
			asunto: '',
			mensaje: '',
		},
		onSubmit: values => {
			console.log(values);
		},
		validate
	});


	return (
		<div className={styles.formulario}>
			<h1>Formik form</h1>

			<form className={styles.formulario} onSubmit={formik.handleSubmit}>
				<div className={styles.formGroup}>
					<label htmlFor="asunto" className={styles.label}>Asunto</label>
					<input
						type='text'
						id="asunto"
						className={styles.input}

						value={formik.values.asunto}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
					/>
				</div>
				{
					formik.touched.asunto && formik.errors.asunto ?
						<div className={styles.error}>{formik.errors.asunto}</div>
						:
						null
				}
				<div className={styles.formGroup}>
					<label htmlFor="mensaje" className={styles.label}>Mensaje</label>
					<textarea
						id="mensaje"
						className={styles.input}

						value={formik.values.mensaje}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
					></textarea>
				</div>
				{
					formik.touched.mensaje && formik.errors.mensaje ?
						<div className={styles.error}>{formik.errors.mensaje}</div>
						:
						null
				}
				<div className={styles.formGroup}>
					<button
						type="submit"
						className={styles.button}
						disabled={!formik.isValid || !formik.dirty}
					>
						Enviar
					</button>
				</div>
			</form>
		</div>
	);
}

export default FormikForm;
