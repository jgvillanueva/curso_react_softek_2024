import React, {FormEvent, FormEventHandler, useState} from 'react';
import styles from '../Form.module.scss';

type IProps = {
}

function UnControlled(props: IProps) {
	const [error, setError] = useState<string[]>([]);
	const [enviado, setEnviado] = useState(false);

	const validators: {[key: string]: Function} = {
		asunto: (value: string) => value && value.length > 5 ?
			'' : 'El asunto debe tener 5 caracteres',
		mensaje: (value: string) => value && value.length > 15 ?
			'' : 'El mensaje debe tener 15 caracteres',
	}

	const handleSubmit: FormEventHandler<HTMLFormElement> = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		const form = event.target;
		const formData = new FormData(form as HTMLFormElement);

		const _errores: string[] = [];
		formData.forEach((value, key) => {
			if(validators[key]) {
				const respuesta = validators[key](value);
				if (respuesta !== '') {
					_errores.push(respuesta);
				}
			}
		})
		setError(_errores);

		if(_errores.length > 0)
			return;

		setEnviado(true);
		const formJson = Object.fromEntries(formData.entries());
		console.log('Se envía: ', formJson);
	};

	const getErrores = () => {
		return error.map((error, index) =>
			<div key={index} className={styles.error}>{error}</div>
		)
	}

	return (
		<div className="UnControlled">
			<h1>UnControlled</h1>
			<form
				data-testid="form"
				className={styles.formulario}
				onSubmit={handleSubmit}
			>
				<div className={styles.formGroup}>
					<label htmlFor="asunto" className={styles.label}>Asunto</label>
					<input
						type='text'
						name="asunto"
						className={styles.input}
						placeholder="Escribe el asunto del mensaje"
					/>
				</div>
				<div className={styles.formGroup}>
					<label htmlFor="mensaje" className={styles.label}>Mensaje</label>
					<textarea
						name="mensaje"
						className={styles.input}
						placeholder="Escribe el texto del mensaje"
					></textarea>
				</div>
				<div className={styles.formGroup}>
					<label htmlFor='user_id' className={styles.label}>User id</label>
					<select
						name="user_id"
						className={styles.select}
						title="Usuario"
					>
						<option value="11">Carmen</option>
						<option value="12">Claudia</option>
						<option value="13">Jorge</option>
						<option value="14">David</option>
					</select>
				</div>
				{getErrores()}
				<div className={styles.formGroup}>
					<button
						type="submit"
						className={styles.button}
					>
						Enviar
					</button>
				</div>

			</form>
			{
				enviado ?
					<div>Se ha enviado el formulario!</div>
					:
					null
			}
		</div>
	);
}

export default UnControlled;
