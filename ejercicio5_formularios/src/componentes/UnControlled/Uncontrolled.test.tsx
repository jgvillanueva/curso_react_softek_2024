import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import UnControlled from "./UnControlled";

const asuntoValue = 'Mi asunto';
const mensajeValue = 'Mi mensaje';
const usuarioValue = '12';

describe('test form', () => {
	it('renders learn react link', () => {
		render(<UnControlled />);
		const title = screen.getByText(/UnControlled/i);
		expect(title).toBeInTheDocument();
	});

	it('renders all elements on form', async () => {
		render(<UnControlled />);
		const asunto: HTMLInputElement = screen.getByPlaceholderText(/Escribe el asunto del mensaje/i);
		expect(asunto).toBeInTheDocument();
		const mensaje: HTMLTextAreaElement = screen.getByPlaceholderText(/Escribe el texto del mensaje/i);
		expect(mensaje).toBeInTheDocument();
		const usuario: HTMLSelectElement = screen.getByTitle(/Usuario/i);
		expect(usuario).toBeInTheDocument();
		expect(asunto).toHaveValue('');
		expect(mensaje).toHaveValue('');

		const boton = screen.getByRole('button');
		fireEvent.click(boton);
		const errorAsunto = screen.getByText('El asunto debe tener 5 caracteres');
		expect(errorAsunto).toBeInTheDocument();
		const errorMensaje = screen.getByText('El mensaje debe tener 15 caracteres');
		expect(errorMensaje).toBeInTheDocument();

		asunto.value = asuntoValue;
		mensaje.value = mensajeValue;
		usuario.value = usuarioValue;

		const selectedUser: HTMLOptionElement = screen.getByText(/Claudia/i);
		expect(selectedUser).toBeInTheDocument();
		expect(selectedUser.selected).toBeTruthy();

		const form = screen.getByTestId('form');
		expect(form).toHaveFormValues({
			asunto: asuntoValue,
			mensaje: mensajeValue,
			user_id: usuarioValue,
		});
	});
});

