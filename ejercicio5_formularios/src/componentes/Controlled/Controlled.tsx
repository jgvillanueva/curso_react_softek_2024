import React, {FormEvent, useState} from 'react';
import styles from '../Form.module.scss';
import ErrorComponent from "../errorComponent/ErrorComponent";
import {Trans, useTranslation} from "react-i18next";

type IProps = {
}

enum Fields {
	asunto = 'asunto',
	mensaje = 'mensaje',
}

interface Mensaje {
	asunto: string,
	mensaje: string,
	user_id: string,
}
interface Errores {
	asunto: string,
	mensaje: string,
}

interface ControlValidacion {
	asunto: boolean,
	mensaje: boolean,
}

function Controlled(props: IProps) {
	const [data, setData] = useState<Mensaje>({
		asunto: '',
		mensaje: '',
		user_id: '12',
	});

	const [errores, setErrores] = useState<Errores>({
		asunto: '',
		mensaje: '',
	});

	const [dirty, setdirty] = useState<ControlValidacion>({
		asunto: false,
		mensaje: false,
	});

	const [formValid, setFormValid] = useState(false);

	const {t} = useTranslation();
	const validaForm = (newErrores: Errores, newDirty: ControlValidacion) => {
		let valido: boolean = true;
		Object.keys(newErrores).forEach((name) => {
			const campo: Fields = name as Fields;
			if (!newDirty[campo] || (newErrores[campo] !== '' && newDirty[campo])) {
				valido = false;
			}
		});
		setFormValid(valido);
	}

	const validacion = (nombre: Fields, valor: string) => {
		const newErrores: Errores = {...errores};
		let resultado = '';
		let valido;
		switch (nombre) {
			case 'asunto':
				valido = valor && valor.length > 5;
				resultado = valido ? '' : t('error_asunto');
				break;
			case 'mensaje':
				valido = valor && valor.length > 15;
				resultado = valido ? '' : t('error_mensaje');
				break;
			default:
				break;
		}
		newErrores[nombre] = resultado;
		setErrores(newErrores);

		validaForm(newErrores, dirty);
	}

	const handleChange = (event: FormEvent) => {
		const newData: Mensaje = {...data};
		const target: HTMLInputElement = event.target as HTMLInputElement;
		const name = target.name as Fields;
		newData[name] = target.value;
		setData(newData);

		validacion(name, target.value);
	}

	const handleBlur = (event: FormEvent) => {
		const target: HTMLInputElement = event.target as HTMLInputElement;
		const name = target.name as Fields;

		const newDirty: ControlValidacion = {...dirty};
		newDirty[name] = true;
		setdirty(newDirty);

		validaForm(errores, newDirty);
	}

	const handleSubmit = (event: FormEvent) => {
		event.preventDefault();
		console.log(data);
	}

	return (
		<div className="Controlled">
			<h1>
				<Trans>title</Trans>
			</h1>
			<form className={styles.formulario} onSubmit={handleSubmit}>
				<div className={styles.formGroup}>
					<label htmlFor="asunto" className={styles.label}>
						<Trans>label_asunto</Trans>
					</label>
					<input
						type='text'
						name="asunto"
						className={styles.input}
						value={data.asunto}
						onChange={handleChange}
						onBlur={handleBlur}
					/>
				</div>
				<ErrorComponent error={errores.asunto} dirty={dirty.asunto} />

				<div className={styles.formGroup}>
					<label htmlFor="mensaje" className={styles.label}>
						<Trans>label_mensaje</Trans>
					</label>
					<textarea
						name="mensaje"
						className={styles.input}
						value={data.mensaje}
						onChange={handleChange}
						onBlur={handleBlur}
					></textarea>
				</div>
				<ErrorComponent error={errores.mensaje} dirty={dirty.asunto}/>

				<div className={styles.formGroup}>
					<label htmlFor='user_id' className={styles.label}>User id</label>
					<select
						name="user_id"
						className={styles.select}
						value={data.user_id}
						onChange={handleChange}
					>
						<option value="11">Carmen</option>
						<option value="12">Claudia</option>
						<option value="13">Jorge</option>
						<option value="14">David</option>
					</select>
				</div>
				<div className={styles.formGroup}>
					<button
						type="submit"
						className={styles.button}
						disabled={!formValid}
					>
						Enviar
					</button>
				</div>
			</form>
		</div>
	);
}

export default Controlled;
