import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import Toggle from "./Toggle";
import styles from './Toggle.module.scss';

const label ='tipo';
const option1 ='tipo1';
const option2 ='tipo2';
const initialValue = true;
const disabled = false;
let returnedValue: boolean;
const handleToggle = (value: boolean) => {
	returnedValue = value;
}

describe('Toggle component', () => {
	beforeEach(() => {
		render(<Toggle
			default={initialValue}
			label={label}
			disabled={disabled}
			opcion1={option1}
			opcion2={option2}
			handleToggle={handleToggle} />);
	})
	it('renders title and option selected', () => {
		const labelElement = screen.getByText(label);
		expect(labelElement).toBeInTheDocument();
	})

	it('click changes', () => {
		const button = screen.getByRole('button');
		expect(button).toBeInTheDocument();
		expect(button).not.toHaveAttribute('disabled');

		expect(button).toHaveClass(styles.off);

		expect(button).toHaveTextContent(option2);

		fireEvent.click(button);
		expect(button).toHaveTextContent(option1);
		expect(button).toHaveClass(styles.on);

		fireEvent.click(button);
		expect(button).toHaveTextContent(option2);

		expect(returnedValue).toEqual(true);
	})
})
