import React, {useState} from 'react';
import styles from './Toggle.module.scss';

type ToogleProps = {
	default: boolean,
	label: string,
	disabled: boolean,
	opcion1: string,
	opcion2: string,
	handleToggle: Function,
}

function Toggle(props: ToogleProps) {
	const [toggle, setToggle] = useState<boolean>(props.default);

	const handleClick = () => {
		const newToggleValue = !toggle;
		setToggle(newToggleValue);
		props.handleToggle(newToggleValue);
	}

	const getButtonClass = (): string => {
		const conlorClass = toggle ? styles.off : styles.on;
		const disabledClass = props.disabled ? styles.disabled : '';
		return `${styles.button} ${conlorClass} ${disabledClass}`;
	}

	return (
		<div className={styles.toggle}>
			<span className={styles.label}>{props.label}</span>
			<button
				onClick={handleClick}
				className={getButtonClass()}
				disabled={props.disabled}
			>
				{
					toggle ?
						<span>{props.opcion2}</span>
						:
						<span>{props.opcion1}</span>
				}
			</button>
		</div>
	);
}

export default Toggle;
