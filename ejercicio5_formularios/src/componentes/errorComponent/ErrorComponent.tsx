import styles from '../Form.module.scss';

type IProps = {
	error: string,
	dirty: boolean,
}

function ErrorComponent (props: IProps) {
	if (props.error === '' || !props.dirty) {
		return null;
	}

	return (
		<div className={styles.error}>
			{props.error}
		</div>
	);
}
export default ErrorComponent;
